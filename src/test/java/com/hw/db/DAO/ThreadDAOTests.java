package com.hw.db.DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ThreadDAOTests {
    String SELECT = "SELECT * FROM \"posts\" WHERE thread = ?  ";
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ThreadDAO thread = new ThreadDAO(mockJdbc);

    @Test
    void treeSortTest1() {
        ThreadDAO.treeSort(1, 2, 3, null);
        verify(mockJdbc).query(
            Mockito.eq(
                SELECT +
                "AND branch > (SELECT branch  FROM posts WHERE id = ?)  " +
                "ORDER BY branch LIMIT ? ;"
            ),
            Mockito.any(PostDAO.PostMapper.class),
            Mockito.eq(1),
            Mockito.eq(3),
            Mockito.eq(2)
        );
    }

    @Test
    void treeSortTest2() {
        ThreadDAO.treeSort(1, 2, 3, true);
        verify(mockJdbc).query(
            Mockito.eq(
                SELECT +
                "AND branch < (SELECT branch  FROM posts WHERE id = ?)  " +
                "ORDER BY branch DESC  LIMIT ? ;"
            ),
            Mockito.any(PostDAO.PostMapper.class),
            Mockito.eq(1),
            Mockito.eq(3),
            Mockito.eq(2)
        );
    }
}