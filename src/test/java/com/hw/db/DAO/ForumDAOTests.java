package com.hw.db.DAO;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ForumDAOTests {
    String THREAD_SLUG = "slug";
    String SELECT = "SELECT nickname,fullname,email,about FROM forum_users ";
    String DATE = "10.09.2021";
    JdbcTemplate mockJdbc = mock(JdbcTemplate.class);
    ForumDAO forum = new ForumDAO(mockJdbc);
    UserDAO.UserMapper userMapper = new UserDAO.UserMapper();

    @Test
    void userListTest1() {
        ForumDAO.UserList(THREAD_SLUG, null, null, false);
        verify(mockJdbc).query(
            Mockito.eq(SELECT + "WHERE forum = (?)::citext ORDER BY nickname;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void userListTest2() {
        ForumDAO.UserList(THREAD_SLUG, 1, null, true);
        verify(mockJdbc).query(
            Mockito.eq(SELECT + "WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void userListTest3() {
        ForumDAO.UserList(THREAD_SLUG, 1, null, false);
        verify(mockJdbc).query(
            Mockito.eq(SELECT + "WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void userListTest4() {
        ForumDAO.UserList(THREAD_SLUG, 1, DATE, true);
        verify(mockJdbc).query(
            Mockito.eq(SELECT +
                "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void userListTest5() {
        ForumDAO.UserList(THREAD_SLUG, 1, DATE, true);
        verify(mockJdbc).query(
            Mockito.eq(SELECT +
                "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void userListTest6() {
        ForumDAO.UserList(THREAD_SLUG, null, DATE, false);
        verify(mockJdbc).query(
            Mockito.eq(SELECT +
                "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void userListTest7() {
        ForumDAO.UserList(THREAD_SLUG, 1, DATE, false);
        verify(mockJdbc).query(
            Mockito.eq(SELECT +
                "WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }


    @Test
    void userListTest8() {
        ForumDAO.UserList(THREAD_SLUG, null, DATE, true);
        verify(mockJdbc).query(
            Mockito.eq(SELECT +
                "WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }

    @Test
    void userListTest9() {
        ForumDAO.UserList(THREAD_SLUG, null, null, true);
        verify(mockJdbc).query(
            Mockito.eq(SELECT +
                "WHERE forum = (?)::citext ORDER BY nickname desc;"),
            Mockito.any(Object[].class),
            Mockito.any(UserDAO.UserMapper.class)
        );
    }
}
